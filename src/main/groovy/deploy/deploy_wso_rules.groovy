#!groovy
package deploy

def params_list

node(params.node_name) {
    stage("Prepare parameters") {
        params_list = readFile file: params.path_to_properties
        params_list = Eval.me(params_list)
        echo("Got parameters: ${params_list}")
    }

    withEnv(params_list) {
        stage("Deploy rules") {
            sh("sleep 60")
            sh("${params.path_to_source}/source/rcip-configuration/rcip-bin/docker/wso2-brs/deployRuleServices.sh ${params.path_to_source}/source/rcip-brms/rcip-rules/target")

        }
    }

}