#!groovy
package deploy


node(params.node_name) {
    stage("Prepare parameters") {
        params_list = readFile file: params.path_to_properties
        params_list = Eval.me(params_list)
        echo("Got parameters: ${params_list}")
    }
    withEnv(params_list) {
        withEnv(["HOST_IP=${params.HOST_IP}"]) {
            stage("Update services") {
                sh("docker service update rcip_flow-initiation-services --force")
                sh("docker service update rcip_presentation-services --force")
                sh("docker service update rcip_process-services --force")
                sh("docker service update rcip_rest --force")

            }
            stage("Wait for process service") {
                dir("../rcip_devops/rcip_stack") {
                    sh("./waitForProcessService.sh")
                }
            }
        }
    }
}