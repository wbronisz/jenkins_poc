#!groovy
package deploy

def params_list

node(params.node_name) {

    stage("Got parameters") {
        params_list = readFile file: params.path_to_properties
        params_list = Eval.me(params_list)
        echo("Got parameters: ${params_list}")
    }

    withEnv(params_list){
        stage("Update liquibase properties") {
            sh("sed -i 's/localhost/${params.HOST_IP}/' ${params.path_to_source}/source/rcip-configuration/rcip-database/liquibase.properties")

        }
        configFileProvider([configFile(fileId: params.config_file_id, variable: 'config')]) {
            withMaven {
                stage("Update db-core") {
                    sh("sleep 60")
                    sh("mvn -f ${params.path_to_source}/source/rcip-configuration/rcip-database/pom.xml " +
                            "-Dliquibase.promptOnNonLocalDatabase=false " +
                            "-Dliquibase.contexts=default,setup " +
                            "liquibase:update -Pdb-core")
                }

                stage("Update db-attachments") {
                    sh("mvn -f ${params.path_to_source}/source/rcip-configuration/rcip-database/pom.xml " +
                            "-Dliquibase.promptOnNonLocalDatabase=false " +
                            "-Dliquibase.contexts=default " +
                            "liquibase:update -Pdb-attachments")
                }

                stage("Update db-events") {
                    sh("mvn -f ${params.path_to_source}/source/rcip-configuration/rcip-database/pom.xml " +
                            "-Dliquibase.promptOnNonLocalDatabase=false " +
                            "-Dliquibase.contexts=default " +
                            "liquibase:update -Pdb-events")
                }
            }
        }
    }

}