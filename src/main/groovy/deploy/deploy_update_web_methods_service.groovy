#!groovy
package deploy

def params_list
def wm_container

node(params.node_name) {
    stage('Checkout') {
        dir(path_to_source) {
            checkout changelog: false, poll: false, scm:
                    [$class                           : 'GitSCM',
                     branches                         : [[name: "*/${params.release_branch}"]],
                     doGenerateSubmoduleConfigurations: false,
                     submoduleCfg                     : [],
                     userRemoteConfigs                : [[credentialsId: "bitbucket", url: "https://rcip-jenkins@bitbucket.org/RCIP/rcip_dev.git"]]
                    ]
        }
    }
    stage("Get parameters") {
        print(params.node_name)
        params_list = readFile file: params.path_to_properties
        params_list = Eval.me(params_list)
        echo("Got parameters: ${params_list}")
    }

    withEnv(params_list) {
        withEnv(["HOST_IP=${params.HOST_IP}"]) {
            configFileProvider([configFile(fileId: params.env_file_id,
                    targetLocation: "${params.path_to_source}",
                    variable: 'config')]) {
                withMaven {
                    stage("Prepeare /opt/softwareag") {
                        if (!fileExists('/opt/softwareag')) {
                            sh("docker cp \$(docker ps --format '{{.Names}}' | grep wm\\.):/opt/softwareag/. /opt/softwareag")
                        }
                    }
                    stage("Restart wm docker") {
                        dir("../rcip_devops") {
                            sh("docker service scale rcip_wm=0")
                            sh("./rcip_stack/waitForWmToBeRemoved.sh")
                            sh("docker volume prune -f")
                            sh("docker service scale rcip_wm=1")
                        }
                    }

                    stage("Wait for wm") {
                        dir("../rcip_devops/rcip_stack") {
                            sh("./waitForWm.sh")
                        }


                    }

                    stage("Install EDA events") {
                        wm_container = sh(
                                script:"docker ps --format '{{.Names}}' | grep rcip_wm",
                                returnStdout: true).trim()
                        def eda_directory = "${params.path_to_source}/source/rcip-interfaces-definitions/src/main/resources/interfaces/midleware/canonical/events/Event Types"
                        def eda_destination = "/opt/softwareag/common/EventTypeStore"

                        try {
                            sh("docker pause \$(docker ps --format '{{.Names}}' | grep rcip_wm)")
                        } catch (err) {
                            print("Caught ${err}")
                        } finally {
                            sh("docker cp '${eda_directory}/.' ${wm_container}:${eda_destination}")
                            sh("docker unpause \$(docker ps --format '{{.Names}}' | grep rcip_wm)")
                        }
                    }

                    stage("Install osgi bundles") {
                        def osgi_bundle_file_name = "${params.path_to_source}/source/rcip-configuration/rcip-bin/docker/WM/bundles"
                        def osgi_destination = "/opt/softwareag/profiles/IS_default/workspace/app-platform/deployer"
                        sh("docker cp ${osgi_bundle_file_name} ${wm_container}:${osgi_destination}")
                    }

                    stage("Install task engine bundle") {
                        def task_engine_bundle_file_name = "${params.path_to_source}/source/rcip-configuration/rcip-bin/docker/WM/RCIPUserTasksGUI.war"
                        def task_engine_destination = "/opt/softwareag/MWS/server/default/deploy"
                        sh("docker cp ${task_engine_bundle_file_name} ${wm_container}:${task_engine_destination}")
                    }

                    stage("Install rcip skin") {
                        def rcip_skin_dir = "${params.path_to_source}/source/rcip-configuration/rcip-bin/docker/WM/skin/RCIP"
                        def skin_destination = "/opt/softwareag/MWS/server/default/deploy/portal.war/ui/skins/"
                        sh("docker cp ${rcip_skin_dir} ${wm_container}:${skin_destination}")
                        sh("docker exec ${wm_container} chown -R wm:wm /opt/softwareag/MWS/server/default/deploy/portal.war/ui/skins/RCIP")
                    }

                    stage("Install WM and RCIP packages") {
                        def rcip_package_dir = "${params.path_to_source}/source/rcip-wm/rcip-wm-esb/RCIP_Packages"
                        sh("sudo cp -r ${rcip_package_dir}/* /var/lib/docker/volumes/rcip_RCIP_Packages/_data/")
                        sh("docker exec ${wm_container} chown -R wm:wm /opt/softwareag/IntegrationServer/instances/default/packages")
                    }

                    stage("Install service") {
                        sh("mvn -f ${params.path_to_source}/source/rcip-wm/pom.xml " +
                                "-Dwm.ABE.output.dir=${params.rcip_abe_output_dir} " +
                                "-Dis.hostname=${params.HOST_IP} " +
                                "-Dis.username=${params.is_login} " +
                                "-Dis.password=${params.is_password} " +
                                "-Dis.port=${params.IS_Port} " +
                                "-Dwm.home.dir=${params.wm_install_dir} " +
                                "-Dprocess.build.config=target/classes/processConfig.xml " +
                                "-Dwm.composites.repository.dir=/tmp/rcip/deploy " +
                                "-Dum.url=${params.um_url} " +
                                "-Dum.port=${params.um_port} " +
                                "-Ddeploy=true " +
                                "clean resources:resources install")
                    }

                    stage("Copy canonical model") {
                        sh("mvn -f ${params.path_to_source}/source/rcip-interfaces-definitions/pom.xml " +
                                "-Dunpack.canonical.model.dir=remote " +
                                "clean install")
                        sh("docker cp ${params.path_to_source}/source/rcip-interfaces-definitions/src/main/resources/interfaces/common ${wm_container}:/opt/softwareag/IntegrationServer/instances/default/packages/RCIP_Canonical_Model/resources/")
                        sh("docker cp ${params.path_to_source}/source/rcip-interfaces-definitions/src/main/resources/interfaces/midleware ${wm_container}:/opt/softwareag/IntegrationServer/instances/default/packages/RCIP_Canonical_Model/resources/")
                        sh("docker cp ${params.path_to_source}/source/rcip-interfaces-definitions/src/main/resources/interfaces ${wm_container}:/opt/softwareag/IntegrationServer/instances/default/packages/RCIP_Utils_Generation/wsdl/")
                        sh("docker cp ${params.path_to_source}/source/rcip-interfaces-definitions/src/main/resources/apigenPrefixes.ini ${wm_container}:/opt/softwareag/IntegrationServer/instances/default/packages/RCIP_Utils_Generation/config")
                        sh("docker cp ${params.path_to_source}/source/rcip-interfaces-definitions/src/main/resources/allConsumers ${wm_container}:/opt/softwareag/IntegrationServer/instances/default/packages/RCIP_Utils_Generation/config")
                    }
                    stage("Restart IS service") {
                        wm_container = sh(
                                script:"docker ps --format '{{.Names}}' | grep rcip_wm",
                                returnStdout: true).trim()
                        sh("docker exec ${wm_container} sh -c \"runuser -l wm -c '/opt/softwareag/profiles/IS_default/bin/restart.sh'\"")
                        sh("${params.path_to_source}/source/rcip-configuration/rcip-bin/docker/WM/wmWaitForISInstance.sh")
                    }
                }
            }
        }

    }
}