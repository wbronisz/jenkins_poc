package deploy

def path_to_source
def path_to_properties
def hosts = [
        "rcip.dev":"138.201.174.153",
        "rcip.qa":"138.201.174.152",
        "rcip.tests":"94.130.26.227",
        "AWS_RC":"172.31.4.114",
"RCIP_SNS":"172.31.4.219"]
def host_ip
def dest_env_ip
node(params.node_name) {
    stage("Set paths") {
        dir("..") {
            main_path = pwd()
            path_to_source = main_path + "/rcip_code"
            path_to_properties = main_path + "/build_parameters.parameters"
        }
    }

    stage('Checkout') {
        dir(path_to_source) {
            checkout changelog: false, poll: false, scm:
                    [$class                           : 'GitSCM',
                     branches                         : [[name: "*/${params.release_branch}"]],
                     doGenerateSubmoduleConfigurations: false,
                     submoduleCfg                     : [],
                     userRemoteConfigs                : [[credentialsId: "bitbucket", url: "https://rcip-jenkins@bitbucket.org/RCIP/rcip_dev.git"]]
                    ]
        }
    }

    stage("Prepare parameters") {
        host_ip = hosts.get(params.node_name)
        print("host_ip set to ${host_ip}")
        dest_env_ip = hosts.get(params.node_name)
        print("dest_env_ip set to ${dest_env_ip}")
        writeFile file: params.path_to_properties, text: "${params}"
        sh("sed -i -e 's/:/=/g' -e 's/\\([a-zA-Z0-9\\_\\.\\:=\\/]*\\)/\"&\"/g' -e 's/\"\"//g' ${path_to_properties}")

    }


    stage("Prepare artifacts") {
        build job: "seed_build_prepare_artifacts", parameters: [string(name: 'path_to_properties', value: params.path_to_properties),
                                                                string(name: 'path_to_source', value: params.path_to_source),
                                                                string(name: 'node_name', value: params.node_name),
                                                                string(name: 'release_branch', value: params.release_branch),
                                                                string(name: 'release_package', value: params.release_package),
                                                                string(name: 'env_file_id', value: params.env_file_id)]
    }

    stage('Build dockers') {
        build job: "seed_build_dockers", parameters: [string(name: 'HOST_IP', value: host_ip),
                                                      string(name: 'path_to_properties', value: path_to_properties),
                                                      string(name: 'path_to_source', value: path_to_source),
                                                      string(name: 'node_name', value: params.node_name),
                                                      string(name: 'config_file_id', value: params.config_file_id),
                                                      string(name: 'env_file_id', value: params.env_file_id),
                                                      string(name: 'keystore_file_id', value: params.keystore_file_id)]
    }
    stage("Deploy services") {
        build job: "seed_deploy_run_services", parameters: [string(name: 'HOST_IP', value: host_ip),
                                                            string(name: 'dest.env.ip', value: dest_env_ip),
                                                            string(name: 'path_to_properties', value: path_to_properties),
                                                            string(name: 'path_to_source', value: path_to_source),
                                                            string(name: 'node_name', value: params.node_name),
                                                            string(name: 'wm_image', value: params.wm_image),
                                                            string(name: 'oracle_image', value: params.oracle_image),
                                                            string(name: 'ldap_image', value: params.ldap_image),
                                                            string(name: 'memcached_image', value: params.memcached_image),
                                                            string(name: 'wso2_server_image', value: params.wso2_server_image),
                                                            string(name: 'flow_image', value: params.flow_image),
                                                            string(name: 'presentation_image', value: params.presentation_image),
                                                            string(name: 'rest_image', value: params.rest_image),
                                                            string(name: 'process_image', value: params.process_image),
                                                            string(name: 'sts_image', value: params.sts_image),
                                                            string(name: 'mock_image', value: params.mock_image),
                                                            string(name: 'ui_demo_image', value: params.ui_demo_image),
                                                            string(name: 'ui_config_image', value: params.ui_config_image),
                                                            string(name: 'jackrabbit_image', value: params.jackrabbit_image),
                                                            string(name: 'mashzone_image', value: params.mashzone_image)
        ]
    }

    stage("Deploy wso rules") {
        build job: "seed_deploy_wso_rules", parameters: [string(name: 'path_to_properties', value: path_to_properties),
                                                         string(name: 'path_to_source', value: path_to_source),
                                                         string(name: 'node_name', value: params.node_name)]
    }

    stage("Update database") {
        build job: "seed_deploy_update_database", parameters: [string(name: 'HOST_IP', value: host_ip),
                                                               string(name: 'path_to_properties', value: path_to_properties),
                                                               string(name: 'path_to_source', value: path_to_source),
                                                               string(name: 'node_name', value: params.node_name),
                                                               string(name: 'config_file_id', value: params.config_file_id)]
    }

    stage("Update wm") {
        build job: "seed_deploy_update_web_methods_service", parameters: [string(name: 'HOST_IP', value: host_ip),
                                                                          string(name: 'path_to_properties', value: params.path_to_properties),
                                                                          string(name: 'path_to_source', value: params.path_to_source),
                                                                          string(name: 'node_name', value: params.node_name),
                                                                          string(name: 'rcip_abe_output_dir', value: params.rcip_abe_output_dir),
                                                                          string(name: 'IS_Port', value: params.IS_Port),
                                                                          string(name: 'release_branch', value: params.release_branch),
                                                                          string(name: 'webmethods_config_file_id', value: params.env_file_id)]
    }
    stage("Update apigateway"){
        build job: "seed_deploy_update_apigateway", parameters: [string(name: 'HOST_IP', value: host_ip),
                                                          string(name: 'node_name', value: params.node_name),
                                                                 string(name: 'path_to_source', value: path_to_source),
                                                                 string(name: 'rcip_gateway_abe_output_dir', value: params.rcip_gateway_abe_output_dir)]
    }
    stage("Update services") {
        build job: "seed_deploy_update_services", parameters: [string(name: 'HOST_IP', value: host_ip),
                                                               string(name: 'path_to_properties', value: params.path_to_properties),
                                                               string(name: 'path_to_source', value: params.path_to_source),
                                                               string(name: 'node_name', value: params.node_name)]
    }

    stage('Run integration tests') {

    }

    stage('Run QA tests') {
        try{
            build job: "seed_qa_tests", parameters: [string(name: 'environment', value: host_ip),
                                                     string(name: 'project_name', value: params.test_project_name),
                                                     string(name: 'release_version', value: params.release_version),
                                                     string(name: 'release_branch', value: params.test_release_branch)]
        } catch (err) {
            print("Caught: ${err}")
            currentBuild.result = 'UNSTABLE'
        }
    }
}