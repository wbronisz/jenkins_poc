#!groovy
package deploy

node(params.node_name) {
    withEnv(["HOST_IP=${params.HOST_IP}", "dest.env.ip=${params.HOST_IP}"]) {
        configFileProvider([configFile(fileId: "1538e4a9-e419-4c7a-b645-8654e421306d",
                targetLocation: "${params.path_to_source}",
                variable: 'config')]) {
            stage("Prepare abe output directory") {
                sh("mkdir -p ${params.rcip_gateway_abe_output_dir}")
            }
            withMaven {
                stage("Install service") {
                        sh("mvn -f ${params.path_to_source}/source/rcip-wm/pom.xml " +
                                "-Dwm.ABE.output.dir=${params.rcip_gateway_abe_output_dir} " +
                                "-Dwm.home.dir=/opt/softwareag " +
                                "-Dis.hostname=${params.HOST_IP} " +
                                "-Dis.port=5556 " +
                                "-Dtarget.is.port=5555 " +
                                "-Dis.username=Administrator " +
                                "-Dis.password=manage " +
                                "-Dwm.package.dir=/tmp/rcip_abe_output " +
                                "-Ddeploy=true " +
                                "-Pdeploy-wm-assets " +
                                "clean resources:resources install")
                }
            }
        }
    }
}