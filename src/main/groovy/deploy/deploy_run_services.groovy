#!groovy
package deploy

def env_params = []

node (params.node_name) {
    stage("Prepare parameters") {
        params_list = readFile file: params.path_to_properties
        params_list = Eval.me(params_list)
        echo("Got parameters from file: ${params_list}")
        params.each() { k -> env_params.add(k.toString())}
        echo("Got parameters from build set up: ${env_params}")
    }
    stage("Checkout"){
        checkout changelog: false, poll: false, scm:
                [$class: 'GitSCM',
                 branches: [[name: "master"]],
                 doGenerateSubmoduleConfigurations: false,
                 extensions: [[$class: 'RelativeTargetDirectory',
                               relativeTargetDir: '../rcip_devops']],
                 submoduleCfg: [],
                 userRemoteConfigs: [[credentialsId: "bitbucket", url: "https://rcip-jenkins@bitbucket.org/RCIP/rcip_devops.git"]]
                ]
    }
    stage("Checkout jks and certs") {
        checkout changelog: false, poll: false, scm:
                [$class                           : 'GitSCM',
                 branches                         : [[name: "master"]],
                 doGenerateSubmoduleConfigurations: false,
                 extensions                       : [[$class           : 'RelativeTargetDirectory',
                                                      relativeTargetDir: '../rcip_keystore']],
                 submoduleCfg                     : [],
                 userRemoteConfigs                : [[credentialsId: "bitbucket", url: "https://rcip-jenkins@bitbucket.org/RCIP/rcip_dev_keystore.git"]]
                ]
        dir("../rcip_keystore/mp_dev") {
            keystore_dir = pwd()
        }
    }
    withEnv(params_list){
        withEnv(env_params){
            dir("../"){

                stage("Prepare workspace") {
                    sh("mkdir -p \$rcip_abe_output_dir")
                }

                stage("Remove exited containers") {
                    try{
                        sh("docker rm \$(docker ps -aqf status=exited)")
                    } catch (e) {
                        print("Caught: ${e}")
                    }

                }

                stage("Deploy services") {
                    def keystore_dir
                    dir("rcip_keystore/rcip_dev"){
                        keystore_dir = pwd()
                    }
                    withEnv(["KEYSTORE_DIR=${keystore_dir}"]){
                        sh("docker stack deploy -c rcip_devops/rcip_stack/rcip.yml rcip")
                    }
                }
            }

        }
    }
}