#!groovy
package aws

node("rcip.qa"){
    stage("Starting instance") {
        sh("~/.local/bin/aws ec2 start-instances --instance-ids '${params.instanceID}'")
    }
}