#!groovy
package aws

def env_params = []

node("rcip.qa") {
    stage("Prepare parameters") {
        params.each() { k -> env_params.add(k.toString()) }
        echo("Got parameters from build set up: ${env_params}")
    }
    stage("Checkout") {
        checkout changelog: false, poll: false, scm:
                [$class                           : 'GitSCM',
                 branches                         : [[name: "master"]],
                 doGenerateSubmoduleConfigurations: false,
                 extensions                       : [[$class           : 'RelativeTargetDirectory',
                                                      relativeTargetDir: '../rcip_devops']],
                 submoduleCfg                     : [],
                 userRemoteConfigs                : [[credentialsId: "bitbucket", url: "https://rcip-jenkins@bitbucket.org/RCIP/rcip_devops.git"]]
                ]
    }
    withEnv(env_params) {
        dir("../rcip_devops/ansible-deploy") {
            stage("Restarting stack") {
                sh("ansible-playbook -i hosts demo-restart-stack.yml")
            }
        }
    }
}