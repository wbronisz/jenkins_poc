#!groovy
package build

def params_list

node(params.node_name) {
    stage("Get parameters") {
        params_list = readFile file: params.path_to_properties
        params_list = Eval.me(params_list)
        echo("Got parameters: ${params_list}")
    }

    withEnv(params_list) {
        configFileProvider([configFile(fileId: params.env_file_id, variable: 'config')]) {
                stage("Build admin ui") {
                    sh("${params.path_to_source}/source/${params.project_name}/rcip-admin-ui/build.sh ${params.path_to_source}/source/${params.project_name}/rcip-admin-ui")
                }
                stage("Build docker") {
                    sh("docker build -t rcip/config-ui ${params.path_to_source}/source/${params.project_name}/rcip-admin-ui/docker/")
            }
        }
    }
}