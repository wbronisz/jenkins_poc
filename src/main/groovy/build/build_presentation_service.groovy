#!groovy
package build

def project_path = "${params.path_to_source}/source/rcip-backends/rcip-presentation-services"

node(params.node_name) {

    stage("Build docker") {
        withMaven {
            stage("Build docker") {
                dir("${project_path}") {
                    sh("mvn -Denv.keystorePath=/etc/rcip-presentation-services/rcip_dev_keystore/ " +
                            "-Denv=rcip_dev " +
                            " clean validate deploy docker:build")
                }
            }
        }
    }
}