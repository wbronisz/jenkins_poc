#!groovy
package build

def params_list
node(params.node_name) {
    stage("Get parameters") {
        params_list = readFile file: params.path_to_properties
        params_list = Eval.me(params_list)
        echo("Got parameters: ${params_list}")
    }
    stage('Checkout') {
        dir(path_to_source) {
            checkout changelog: false, poll: false, scm:
                    [$class                           : 'GitSCM',
                     branches                         : [[name: "*/${params.release_branch}"]],
                     doGenerateSubmoduleConfigurations: false,
                     extensions                       : [[$class           : 'RelativeTargetDirectory',
                                                          relativeTargetDir: "${params.path_to_source}"]],
                     submoduleCfg                     : [],
                     userRemoteConfigs                : [[credentialsId: "bitbucket", url: "https://rcip-jenkins@bitbucket.org/RCIP/rcip_dev.git"]]
                    ]
        }
    }
    withEnv(params_list) {
        dir(params.path_to_source) {
            stage("Install") {
                sh("docker stack rm rcip")
                sh("mvn -f source/pom.xml clean deploy -U -Dcreate.release.package=${params.release_package}")
            }
        }
    }
}
