#!groovy
package build

def project_path = "${params.path_to_source}/source/rcip-rest"
def params_list
def env_params = []
node(params.node_name) {
    stage("Get parameters") {
        params.each() { k -> env_params.add(k.toString())}
        params_list = readFile file: params.path_to_properties
        params_list = Eval.me(params_list)

        echo("Got parameters: ${params_list}")
    }

    withEnv(env_params){
        withEnv(params_list) {
            withMaven {
                stage("Build docker") {
                    dir("${project_path}") {
                        sh("mvn " +
                                "-Dport=$restApiPort" +
                                "-DstsPort=$stsPort " +
                                "-Dproject_name=$project_name " +
                                "-Denv=rcip_dev " +
                                "-Denv.keystorePath=/etc/rcip-rest/rcip_dev_keystore/ " +
                                " clean validate deploy docker:build")
                    }
                }
            }
        }
    }
}