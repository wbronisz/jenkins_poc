#!groovy
package build

def project_path = "${params.path_to_source}/source/rcip-backends/rcip-flow-initiation-services"

node(params.node_name) {

    stage("Build docker") {
        withMaven {
            stage("Build docker") {
                dir("${project_path}") {
                    sh("mvn -Denv.keystorePath=/etc/rcip-flow-initiation-services/rcip_dev_keystore/ " +
                            "-Denv=rcip_dev " +
                            " clean validate deploy docker:build")
                }
            }
        }
    }
}