package build

node(params.node_name) {
    stage("Checkout and copy jks and certs") {
        checkout changelog: false, poll: false, scm:
                [$class: 'GitSCM',
                 branches: [[name: "master"]],
                 doGenerateSubmoduleConfigurations: false,
                 extensions: [[$class: 'RelativeTargetDirectory',
                               relativeTargetDir: '../rcip_keystore']],
                 submoduleCfg: [],
                 userRemoteConfigs: [[credentialsId: "bitbucket", url: "https://rcip-jenkins@bitbucket.org/RCIP/rcip_dev_keystore.git"]]
                ]
    }
    stage("Build dockers") {
        print("HOST_IP set to ${params.HOST_IP}")
        parallel(
                "process services": {
                    build job: "seed_build_process_service", parameters: [string(name: 'path_to_properties', value: params.path_to_properties),
                                                                          string(name: 'path_to_source', value: params.path_to_source),
                                                                          string(name: 'node_name', value: params.node_name),
                                                                          string(name: 'config_file_id', value: params.config_file_id)]
                },
                "security token": {
                    build job: "seed_build_security_token_service", parameters: [string(name: 'HOST_IP', value: params.HOST_IP),
                                                                                 string(name: 'path_to_properties', value: params.path_to_properties),
                                                                                 string(name: 'path_to_source', value: params.path_to_source),
                                                                                 string(name: 'node_name', value: params.node_name),
                                                                                 string(name: 'config_file_id', value: params.config_file_id),
                                                                                 string(name: 'keystore_file_id', value: params.keystore_file_id)]
                },
                "presentation": {
                    build job: "seed_build_presentation_service", parameters: [string(name: 'path_to_properties', value: params.path_to_properties),
                                                                               string(name: 'path_to_source', value: params.path_to_source),
                                                                               string(name: 'node_name', value: params.node_name),
                                                                               string(name: 'config_file_id', value: params.config_file_id)]
                },
                "rest": {
                    build job: "seed_build_rest_service", parameters: [string(name: 'path_to_properties', value: params.path_to_properties),
                                                                       string(name: 'path_to_source', value: params.path_to_source),
                                                                       string(name: 'node_name', value: params.node_name),
                                                                       string(name: 'config_file_id', value: params.config_file_id),
                                                                       string(name: 'keystore_file_id', value: params.keystore_file_id)]
                },
                "flow initiation": {
                    build job: "seed_build_flow_initiation_service", parameters: [string(name: 'path_to_properties', value: params.path_to_properties),
                                                                                  string(name: 'path_to_source', value: params.path_to_source),
                                                                                  string(name: 'node_name', value: params.node_name),
                                                                                  string(name: 'config_file_id', value: params.config_file_id)]
                },
                "config ui": {
                    build job: "seed_build_config_ui_service", parameters: [string(name: 'path_to_properties', value: params.path_to_properties),
                                                                                  string(name: 'path_to_source', value: params.path_to_source),
                                                                                  string(name: 'node_name', value: params.node_name),
                                                                                  string(name: 'env_file_id', value: params.env_file_id)]
                }
        )
    }
    stage("Clean dangling") {
        try {
            sh("docker rmi \$(docker images -q -f \"dangling=true\")")
        } catch (ex) {
            print("Got exception: " + ex)
        }
    }
}