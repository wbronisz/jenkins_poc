#!groovy
package build

def params_list
node(params.node_name) {
    stage("Get parameters") {
        params_list = readFile file: params.path_to_properties
        params_list = params_list.split("\\r?\\n");
        echo("Got parameters: ${params_list}")
    }

    withEnv(params_list) {
        configFileProvider([configFile(fileId: params.config_file_id,
                variable: 'config')]) {
            withMaven {
                stage("test maven") {
                    sh("mvn -f ${params.path_to_source}/source/rcip-event-services/pom.xml validate")
                }
            }
        }
    }
}