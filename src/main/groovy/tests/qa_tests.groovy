package tests
def err = null
def env_for_tests = null
node('rcip.qa') {

    //mapping AWS private to public - environment does not have proper communication if ip is set to public on AWS env
    if (params.environment.equals("172.31.13.199")){
        env_for_tests = "34.211.82.251"
    } else {
        env_for_tests = params.environment
    }
    stage('Checkout') {
        checkout changelog: false, poll: false, scm:
                [$class: 'GitSCM',
                 branches: [[name: "*/${params.release_branch}"]],
                 doGenerateSubmoduleConfigurations: false,
                 extensions: [],
                 submoduleCfg: [],
                 userRemoteConfigs: [[credentialsId: "bitbucket", url: "https://rcip-jenkins@bitbucket.org/RCIP/rcip_qa.git"]]
                ]
    }
    try {
        stage('Run tests') {
            dir("chaperone-core/webservices-automation") {
                sh "mvn clean compile com.smartbear.soapui:soapui-pro-maven-plugin:5.1.1:test -DsoapProject=${params.project_name} -Denv=${env_for_tests}"
            }
            currentBuild.result = "SUCCESS"
        }
    } catch (e) {
        err = e
        currentBuild.result = "FAILURE"
    } finally {
        stage('Publish test report') {
            junit 'chaperone-core/webservices-automation/target/reports/report.xml'
            print("result: ${currentBuild.result}")
            if (currentBuild.result != 'SUCCESS') {
                mail to: 'wbronisz@7bulls.com', subject: 'soap regression tests unstable or failed', body: 'soap regression tests unstable or failed'
            }
        }
        if (params.release_version)
            stage('Update Jira') {
                build job: "seed_qa_update_jira", parameters: [string(name: 'release_version', value: params.release_version),
                                                               string(name: 'GIT_BRANCH', value: params.release_branch),
                                                               string(name: 'BUILD_NUMBER', value: Integer.toString(currentBuild.number)),
                                                               string(name: 'project', value: params.project),
                                                               string(name: 'project_test', value: params.project_test),
                                                               string(name: 'report_path', value: WORKSPACE+'/chaperone-core/webservices-automation/target/reports/')], wait: false
            }
        if (err) {
            throw err
        }
    }
}