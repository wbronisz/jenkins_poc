//TODO
package tests
node (params.node) {
    stage ("Get parameters") {
        params_list = readFile file: params.path_to_properties
        params_list = Eval.me(params_list)
        echo("Got parameters: ${params_list}")
    }
    withEnv(params_list) {
        stage('Run tests') {
            sh("mvn -f ${params.path_to_source}/source/rcip-wm/pom.xml" +
                    " -DendpointURL=${params_list.HOST_IP}:${params_list.IS_Port}" +
                    " com.smartbear.soapui:soapui-maven-plugin:test")
        }
    }
    stage('Publish test report') {
        //        junit 'chaperone-core/webservices-automation/target/reports/report.xml' //TODO lokalizacja wyniku testów
        if (currentBuild.result != 'SUCCESS') {
            mail to: 'wbronisz@7bulls.com', subject: 'rcip-wm tests unstable or failed', body: 'rcip-wm tests unstable or failed'
        }
    }
}