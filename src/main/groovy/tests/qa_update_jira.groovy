package tests

node('rcip.qa') {
    def env_params = []
    params.each() { k -> env_params.add(k.toString())}
    withEnv(env_params) {

        stage('Get test report') {
            try {
                sh "cp ${params.report_path}report.xml ."
            } catch (e) {
                print("Got: ${e}")
            }
        }

        stage('Create release version in Jira') {
            sh "../seed_qa_tests/chaperone-core/integration-tools/rcip_release_version.py --project ${params.project} --username rcipjenkins@roundcube.eu --password sniknej-picr --version \"${params.release_version}\""
            sh "../seed_qa_tests/chaperone-core/integration-tools/rcip_release_version.py --project ${params.project_test} --username rcipjenkins@roundcube.eu --password sniknej-picr --version \"${params.release_version}\""
        }

        stage('Update jira') {
            try {
                sh "../seed_qa_tests/chaperone-core/integration-tools/rcip_jenkins_tests_integrated.py --file report.xml --username rcipjenkins@roundcube.eu --password sniknej-picr --version ${params.release_version}"
            } catch (e) {
                print("Got: ${e}")
            }
        }
    }
}