#!groovy
package mp

node(params.node) {
    stage("Checkout") {
        checkout changelog: false, poll: false, scm:
                [$class                           : 'GitSCM',
                 branches                         : [[name: "${params.branch_mp}"]],
                 doGenerateSubmoduleConfigurations: false,
                 extensions                       : [[$class           : 'RelativeTargetDirectory',
                                                      relativeTargetDir: '../mp_dev']],
                 submoduleCfg                     : [],
                 userRemoteConfigs                : [[credentialsId: "bitbucket", url: "https://rcip-jenkins@bitbucket.org/RCIP/mp_dev.git"]]
                ]
    }

    stage("Checkout jks and certs") {
        checkout changelog: false, poll: false, scm:
                [$class: 'GitSCM',
                 branches: [[name: "master"]],
                 doGenerateSubmoduleConfigurations: false,
                 extensions: [[$class: 'RelativeTargetDirectory',
                               relativeTargetDir: '../rcip_keystore']],
                 submoduleCfg: [],
                 userRemoteConfigs: [[credentialsId: "bitbucket", url: "https://rcip-jenkins@bitbucket.org/RCIP/rcip_dev_keystore.git"]]
                ]
        //TODO test
        dir("../") {
            sh("cp -R rcip_keystore/mp_dev/* rcip_keystore")
        }
    }

    stage("Checkout RCIP") {
        checkout changelog: false, poll: false, scm:
                [$class                           : 'GitSCM',
                 branches                         : [[name: "*/${params.branch_rcip}"]],
                 doGenerateSubmoduleConfigurations: false,
                 submoduleCfg                     : [],
                 extensions                       : [[$class           : 'RelativeTargetDirectory',
                                                      relativeTargetDir: '../rcip_dev']],
                 userRemoteConfigs                : [[credentialsId: "bitbucket", url: "https://rcip-jenkins@bitbucket.org/RCIP/rcip_dev.git"]]
                ]
    }

    dir("../mp_dev/source"){
        stage("Clean workspace"){
            sh("mvn clean -Pclean-rcip-canonical-model -pl mp-interfaces-definitions")
            sh("mvn clean -Pclean-rcip-wm-packages -pl mp-wm/mp-wm-esb/")
        }
    }
    stage("Build dockers") {
        parallel(
                "process_service": {
                    build job: "mp_build_process_service", parameters: [string(name: 'path_to_properties', value: params.path_to_properties),
                                                                        string(name: 'path_to_source', value: params.path_to_source),
                                                                        string(name: 'node', value: params.node),
                                                                        string(name: 'config_file_id', value: params.config_file_id)]

                },
                "flow_initiation": {
                    build job: "mp_build_flow_initiation", parameters: [string(name: 'path_to_properties', value: params.path_to_properties),
                                                                        string(name: 'path_to_source', value: params.path_to_source),
                                                                        string(name: 'node', value: params.node),
                                                                        string(name: 'branch_rcip', value: params.branch_rcip),
                                                                        string(name: 'config_file_id', value: params.config_file_id)]
                },
                "presentation_services": {
                    build job: "mp_build_presentation_services", parameters: [string(name: 'path_to_properties', value: params.path_to_properties),
                                                                        string(name: 'path_to_source', value: params.path_to_source),
                                                                        string(name: 'node', value: params.node),
                                                                        string(name: 'branch_rcip', value: params.branch_rcip),
                                                                        string(name: 'config_file_id', value: params.config_file_id)]
                },
                "rest_service": {
                    build job: "mp_build_rest_service", parameters: [string(name: 'path_to_properties', value: params.path_to_properties),
                                                                     string(name: 'path_to_source', value: params.path_to_source),
                                                                     string(name: 'node', value: params.node),
                                                                     string(name: 'branch_rcip', value: params.branch_rcip),
                                                                     string(name: 'config_file_id', value: params.config_file_id)]
                },
                "sts_service": {
                    build job: "seed_build_security_token_service", parameters: [string(name: 'HOST_IP', value: params.HOST_IP),
                                                                                 string(name: 'path_to_source', value: "../rcip_dev"),
                                                                                 string(name: 'keystore_file_id', value: params.keystore_sts_file_id),
                                                                                 string(name: 'node_name', value: params.node)]
                },
                "rest_auth_server": {
                    build job: "mp_build_rest_auth_server", parameters: [string(name: 'path_to_properties', value: params.path_to_properties),
                                                                     string(name: 'path_to_source', value: params.path_to_source),
                                                                     string(name: 'node', value: params.node),
                                                                     string(name: 'config_file_id', value: params.config_file_id)]

                }
        )
    }
    stage("Clean dangling") {
        try {
            sh("docker rmi \$(docker images -q -f \"dangling=true\")")
        } catch (ex) {
            print("Got exception: " + ex)
        }
    }
}
