#!groovy
package mp

node(params.node) {
    stage("Build docker") {
        withMaven {
            stage("Build docker") {
                dir("../rcip_dev/source/rcip-backends/rcip-flow-initiation-services") {
                    sh("mvn -Denv.keystorePath=/etc/rcip-flow-initiation-services/rcip_dev_keystore/ " +
                            "-Denv=mp_dev " +
                            "clean validate deploy docker:build")
                }
            }
        }
    }
}