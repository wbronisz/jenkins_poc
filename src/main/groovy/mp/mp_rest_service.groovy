#!groovy
package mp

node(params.node) {
    stage("Build docker") {
        try {
            withMaven {
                stage("Build docker") {
                    def keystore_dir
                    dir("../rcip_keystore/mp_dev") {
                        keystore_dir = pwd()
                    }
                    dir("../mp_dev/source/mp-rest/mp-rest-api") {
                        sh("mvn -Dport=9093 -Dproject_name=mp-rest-api " +
                                "-Denv.keystorePath=/etc/mp-rest-api/rcip_dev_keystore/ " +
                                "-Denv=mp_dev " +
                                "clean validate deploy docker:build")
                    }
                }
            }

        }
        catch (e) {

            withMaven {
                stage("Build docker") {
                    def keystore_dir
                    dir("../rcip_keystore/mp_dev") {
                        keystore_dir = pwd()
                    }
                    dir("../mp_dev/source/mp-rest-api") {
                        sh("mvn -Dport=9093" +
                                "-DstsPort=9094 " +
                                "-Dproject_name=mp-rest-api " +
                                "clean deploy docker:build")
                    }
                }
            }
        }
    }
}