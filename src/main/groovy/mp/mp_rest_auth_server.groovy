#!groovy
package mp

node(params.node) {
    stage("Build docker") {
        try {
            withMaven {
                stage("Build docker") {
                    dir("../mp_dev/source/mp-rest/mp-rest-auth-server") {
                        sh("mvn -Denv.keystorePath=/etc/mp-rest-auth-server/rcip_dev_keystore/ " +
                                "-Denv=mp_dev " +
                                "clean validate deploy docker:build")
                    }
                }

            }
        } catch (e) {
            print("Got ${e}")
        }
    }
}