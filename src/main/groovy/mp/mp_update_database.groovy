#!groovy
package mp

node(params.node) {
    stage('Checkout RCIP') {
        checkout changelog: false, poll: false, scm:
                [$class                           : 'GitSCM',
                 branches                         : [[name: "*/${params.branch_rcip}"]],
                 doGenerateSubmoduleConfigurations: false,
                 submoduleCfg                     : [],
                 extensions                       : [[$class           : 'RelativeTargetDirectory',
                                                      relativeTargetDir: '../rcip_dev']],
                 userRemoteConfigs                : [[credentialsId: "bitbucket", url: "https://rcip-jenkins@bitbucket.org/RCIP/rcip_dev.git"]]
                ]
    }

    stage("Update liquibase properties") {
        sh("sed -i 's/localhost/${params.HOST_IP}/' ../rcip_dev/source/rcip-configuration/rcip-database/liquibase.properties")

    }
    withMaven {
        dir("../rcip_dev/source/rcip-configuration/rcip-database") {
            stage("Update rcip db-core") {
                sh("sleep 60")
                sh("mvn -Dliquibase.promptOnNonLocalDatabase=false " +
                        "-Dliquibase.contexts=default,setup " +
                        "liquibase:update -Pdb-core")
            }

            stage("Update rcip db-attachments") {
                sh("mvn -Dliquibase.promptOnNonLocalDatabase=false " +
                        "-Dliquibase.contexts=default " +
                        "liquibase:update -Pdb-attachments")
            }

            stage("Update rcip db-events") {
                sh("mvn -Dliquibase.promptOnNonLocalDatabase=false " +
                        "-Dliquibase.contexts=default " +
                        "liquibase:update -Pdb-events")
            }
        }
    }

}