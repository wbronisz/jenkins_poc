#!groovy
package mp

node(params.node) {
    stage("Checkout") {
        checkout changelog: false, poll: false, scm:
                [$class                           : 'GitSCM',
                 branches                         : [[name: "${params.branch_mp}"]],
                 doGenerateSubmoduleConfigurations: false,
                 extensions                       : [[$class           : 'RelativeTargetDirectory',
                                                      relativeTargetDir: '../mp_dev']],
                 submoduleCfg                     : [],
                 userRemoteConfigs                : [[credentialsId: "bitbucket", url: "https://rcip-jenkins@bitbucket.org/RCIP/mp_dev.git"]]
                ]
    }

    stage("Checkout jks and certs") {
        checkout changelog: false, poll: false, scm:
                [$class: 'GitSCM',
                 branches: [[name: "master"]],
                 doGenerateSubmoduleConfigurations: false,
                 extensions: [[$class: 'RelativeTargetDirectory',
                               relativeTargetDir: '../rcip_keystore']],
                 submoduleCfg: [],
                 userRemoteConfigs: [[credentialsId: "bitbucket", url: "https://rcip-jenkins@bitbucket.org/RCIP/rcip_dev_keystore.git"]]
                ]
    }
    stage("Build and push artifacts") {
        dir("../mp_dev/source"){
            sh("mvn clean deploy -U -Dcreate.release.package=${params.release_package}")
        }
    }
}
