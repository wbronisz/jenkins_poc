#!groovy
package mp

node(params.node) {
    stage("Build docker") {
        withMaven {
            dir("../mp_dev/source/mp-backends/mp-process-services") {
                sh("mvn -Denv.keystorePath=/etc/mp-process-services/rcip_dev_keystore/ " +
                        "-Denv=mp_dev " +
                        "clean validate deploy docker:build")
            }
        }
    }
}