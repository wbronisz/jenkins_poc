#!groovy
package mp

def wm_container
def env_params = []

node(params.node) {
    stage("Prepare parameters") {
        params.each() { k -> env_params.add(k.toString()) }
        echo("Got parameters from build set up: ${env_params}")
    }
    stage("Checkout MP") {
        checkout changelog: false, poll: false, scm:
                [$class                           : 'GitSCM',
                 branches                         : [[name: "${params.branch_mp}"]],
                 doGenerateSubmoduleConfigurations: false,
                 extensions                       : [[$class           : 'RelativeTargetDirectory',
                                                      relativeTargetDir: '../mp_dev']],
                 submoduleCfg                     : [],
                 userRemoteConfigs                : [[credentialsId: "bitbucket", url: "https://rcip-jenkins@bitbucket.org/RCIP/mp_dev.git"]]
                ]
    }
    stage('Checkout RCIP') {
        checkout changelog: false, poll: false, scm:
                [$class                           : 'GitSCM',
                 branches                         : [[name: "*/${params.branch_rcip}"]],
                 doGenerateSubmoduleConfigurations: false,
                 submoduleCfg                     : [],
                 extensions                       : [[$class           : 'RelativeTargetDirectory',
                                                      relativeTargetDir: '../rcip_dev']],
                 userRemoteConfigs                : [[credentialsId: "bitbucket", url: "https://rcip-jenkins@bitbucket.org/RCIP/rcip_dev.git"]]
                ]
    }

    withEnv(["HOST_IP=${params.HOST_IP}",
             "dest.env.ip=${params.HOST_IP}"]) {
        configFileProvider([configFile(fileId: "1538e4a9-e419-4c7a-b645-8654e421306d",
                targetLocation: "${params.path_to_source}",
                variable: 'config')]) {
            withMaven {
                stage("Prepeare /opt/softwareag") {
                    if (!fileExists('/opt/softwareag')) {
                        sh("docker cp ${wm_container}:/opt/softwareag/. /opt/softwareag")
                    }
                }
                stage("Restart wm docker") {
                    withEnv(env_params) {
                        dir("../rcip_devops") {
                            sh("docker service scale mp_wm=0")
                            sh("./rcip_stack/waitForWmToBeRemoved.sh")
                            sh("docker volume prune -f")
                            sh("docker service scale mp_wm=1")
                        }
                    }
                }

                wm_container = sh(
                        script: "docker ps --format '{{.Names}}' | grep mp_wm",
                        returnStdout: true
                ).trim()
                stage("Prepare abe output") {
                    sh("mkdir -p ${params.mp_abe_output_dir}")
                }

                stage("Wait for wm") {
                    dir("../rcip_devops/rcip_stack") {
                        sh("./waitForWm.sh")
                    }
                }

                stage("Install osgi bundles") {
                    def osgi_bundle_file_name = "../rcip_dev/source/rcip-configuration/rcip-bin/docker/WM/bundles"
                    def osgi_destination = "/opt/softwareag/profiles/IS_default/workspace/app-platform/deployer"
                    sh("docker cp ${osgi_bundle_file_name} ${wm_container}:${osgi_destination}")
                }

                stage("Install service") {
                    dir("../mp_dev/source/mp-wm/") {

                        sh("mvn -Dwm.ABE.output.dir=${params.mp_abe_output_dir} " +
                                "-Dis.hostname=${params.HOST_IP} " +
                                "-Dis.username=Administrator " +
                                "-Dis.password=manage " +
                                "-Dis.port=5555 " +
                                "-Dwm.home.dir=/opt/softwareag " +
                                "-Dprocess.build.config=target/classes/processConfig.xml " +
                                "-Dwm.composites.repository.dir=/tmp/rcip/deploy " +
                                "-Dum.url=mp-wm " +
                                "-Dum.port=9000 " +
                                "-Ddeploy=true " +
                                "clean resources:resources install")

                        try {
                            sh("mvn clean resources:resources install " +
                                    "-Pdeploy-wm-process " +
                                    "-Dwm.home.dir=/opt/softwareag " +
                                    "-Dis.hostname=localhost " +
                                    "-Dis.port=5555 " +
                                    "-Dis.username=Administrator " +
                                    "-Dis.password=manage " +
                                    "-Dprocess.build.config=src/main/resources/processConfig.xml")
                        } catch (e) {
                            print("Got ${e}")
                        }
                    }
                }

                stage("Install WM and RCIP packages") {

                    def rcip_package_dir = "/home/ubuntu/workspace/mp_dev/source/mp-wm/mp-wm-esb/MP_Packages"
                    sh("sudo cp -r ${rcip_package_dir}/* /var/lib/docker/volumes/mp_RCIP_Packages/_data/")
                    sh("docker exec ${wm_container} chown -R wm:wm /opt/softwareag/IntegrationServer/instances/default/packages")

                }

                stage("copy xsd and wsdls"){
                    dir("../mp_dev/source") {
                        sh("mvn clean install -pl mp-interfaces-definitions")
                    }
                }
                stage("copy to container"){
                    try {
                        def rcip_package_dir = "/home/ubuntu/workspace/mp_dev/source/mp-wm/mp-wm-esb/MP_Packages"
                        def mp_interface_def_dir = "/home/ubuntu/workspace/mp_dev/source/mp-interfaces-definitions/src/main/resources"

                        def rcip_core_canonical = "/opt/softwareag/IntegrationServer/instances/default/packages/MP_Canonical_Model/resources/rcip-core-canonical/"
                        sh("docker exec -it ${wm_container} sh -c 'mkdir -p ${rcip_core_canonical}'")
                        sh("docker cp ${rcip_package_dir}/MP_Canonical_Model/resources/rcip-core-canonical/common ${wm_container}:${rcip_core_canonical}")
                        sh("docker cp ${rcip_package_dir}/MP_Canonical_Model/resources/rcip-core-canonical/middleware ${wm_container}:${rcip_core_canonical}")

                        def resources_mp = "/opt/softwareag/IntegrationServer/instances/default/packages/MP_Canonical_Model/resources/mp/"
                        sh("docker exec -it ${wm_container} sh -c 'mkdir -p ${resources_mp}'")
                        sh("docker cp ${mp_interface_def_dir}/interfaces/middleware ${wm_container}:${resources_mp}")

                        def generation_config = "/opt/softwareag/IntegrationServer/instances/default/packages/RCIP_Utils_Generation/config"
                        sh("docker exec -it ${wm_container} sh -c 'mkdir -p ${generation_config}'")
                        sh("docker cp ${mp_interface_def_dir}/apigenPrefixes.ini ${wm_container}:${generation_config}")
                        sh("docker cp ${mp_interface_def_dir}/allConsumers ${wm_container}:${generation_config}")

                    } catch (e) {
                        print("Got exception: ${e}")
                    }
                }

                stage("Restart IS service") {
                    sh("docker exec ${wm_container} sh -c \"runuser -l wm -c '/opt/softwareag/profiles/IS_default/bin/restart.sh'\"")
                    sh("../rcip_code/source/rcip-configuration/rcip-bin/docker/WM/wmWaitForISInstance.sh")
                }

                stage("Commit image") {
                    sh("docker commit ${wm_container} ${params.wm_image}${params.wm_tag}")
                }
            }
        }
    }
}