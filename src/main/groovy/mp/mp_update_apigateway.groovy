#!groovy
package mp

node(params.node) {
    withEnv(["HOST_IP=${params.HOST_IP}", "dest.env.ip=${params.HOST_IP}"]) {
        configFileProvider([configFile(fileId: "1538e4a9-e419-4c7a-b645-8654e421306d",
                targetLocation: "${params.path_to_source}",
                variable: 'config')]) {
            stage("Checkout MP") {
                checkout changelog: false, poll: false, scm:
                        [$class                           : 'GitSCM',
                         branches                         : [[name: "${params.branch_mp}"]],
                         doGenerateSubmoduleConfigurations: false,
                         extensions                       : [[$class           : 'RelativeTargetDirectory',
                                                              relativeTargetDir: '../mp_dev']],
                         submoduleCfg                     : [],
                         userRemoteConfigs                : [[credentialsId: "bitbucket", url: "https://rcip-jenkins@bitbucket.org/RCIP/mp_dev.git"]]
                        ]
            }
            stage("Prepare abe output directory") {
                sh("mkdir -p ${params.mp_gateway_abe_output_dir}")
            }

            stage("Install service") {
                dir("../mp_dev/source/mp-wm/") {
                    sh("mvn -Dwm.ABE.output.dir=/tmp/apiGatewayDeployerRepo/ " +
                            "-Dwm.home.dir=/opt/softwareag2 " +
                            "-Dis.hostname=localhost " +
                            "-Dis.port=5556 " +
                            "-Dtarget.is.port=5556 " +
                            "-Dsag10.dir=/opt/softwareag2/ " +
                            "-Dis.username=Administrator " +
                            "-Dis.password=manage " +
                            "-Dwm.package.dir=/tmp/apiGatewayDeployerRepo/ " +
                            "-Ddeploy=true " +
                            "-Pdeploy-api-gateway-assets " +
                            "clean resources:resources install")
                }
            }

            stage("Install API Gateway packages") {
                def gateway_container = sh(
                        script: "docker ps --format '{{.Names}}' | grep mp_api-gateway",
                        returnStdout: true
                ).trim()
                def rcip_package_dir = "/home/ubuntu/workspace/mp_dev/source/mp-wm/mp-wm-esb/MP_Packages"
                try {
                    sh("docker cp ${rcip_package_dir}/WmAPIGatewayTools ${gateway_container}:/opt/softwareag/IntegrationServer/instances/default/packages")
                    sh("docker exec ${gateway_container} chown -R wm:wm /opt/softwareag/IntegrationServer/instances/default/packages")
                } catch (e) {
                    print("Got ${e}")
                }

            }
            stage("Commit image") {

                def gateway_container = sh(
                        script: "docker ps --format '{{.Names}}' | grep mp_api-gateway",
                        returnStdout: true
                ).trim()
                sh("docker commit ${gateway_container} ${params.gateway_image}${params.gateway_tag}")
            }
        }
    }
}