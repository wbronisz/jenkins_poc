#!groovy
package mp

def env_params = []
//def keystore_dir = "/home/ubuntu/workspace/rcip_keystore"
def keystore_dir

node(params.node) {
    stage("Prepare parameters") {
        params.each() { k -> env_params.add(k.toString()) }
        echo("Got parameters from build set up: ${env_params}")
    }
    stage("Checkout") {
        checkout changelog: false, poll: false, scm:
                [$class                           : 'GitSCM',
                 branches                         : [[name: "master"]],
                 doGenerateSubmoduleConfigurations: false,
                 extensions                       : [[$class           : 'RelativeTargetDirectory',
                                                      relativeTargetDir: '../rcip_devops']],
                 submoduleCfg                     : [],
                 userRemoteConfigs                : [[credentialsId: "bitbucket", url: "https://rcip-jenkins@bitbucket.org/RCIP/rcip_devops.git"]]
                ]
    }
    stage("Checkout jks and certs") {
        checkout changelog: false, poll: false, scm:
                [$class                           : 'GitSCM',
                 branches                         : [[name: "master"]],
                 doGenerateSubmoduleConfigurations: false,
                 extensions                       : [[$class           : 'RelativeTargetDirectory',
                                                      relativeTargetDir: '../rcip_keystore']],
                 submoduleCfg                     : [],
                 userRemoteConfigs                : [[credentialsId: "bitbucket", url: "https://rcip-jenkins@bitbucket.org/RCIP/rcip_dev_keystore.git"]]
                ]
        dir("../rcip_keystore/mp_dev") {
            keystore_dir = pwd()
        }
    }
    withEnv(env_params) {
        dir("../") {
            stage("Remove old stack") {
                try {
                    sh("docker stack rm mp")
                    sh("sleep 30")
                } catch (e) {
                    print("Caught: ${e}")
                }
            }

            stage("Pull images") {
                sh("docker pull ${params.wm_image}")
                sh("docker pull ${params.ldap_image}")
                sh("docker pull ${params.gateway_image}")
                sh("docker pull 138.201.174.153:15000/rcip/backend-base")
            }

            stage("Tag wm and gateway images for stack") {
                sh("docker tag ${params.wm_image} ${params.wm_image}${params.wm_tag}")
                sh("docker tag ${params.gateway_image} ${params.gateway_image}${params.gateway_tag}")
            }

            stage("Remove exited containers") {
                try {
                    sh("docker rm \$(docker ps -aqf status=exited)")
                } catch (e) {
                    print("Caught: ${e}")
                }
                try {
                    sh("docker rmi -f \$(docker images -f \"dangling=true\" -q)")
                } catch (e) {
                    print("Caught: ${e}")
                }

            }

            stage("Deploy services") {
                sh("sudo mkdir -p /opt/softwareag2")
                sh("sudo chown -R \$(whoami):\$(whoami) /opt/softwareag2")
                sh("chmod g+rws /opt/softwareag2")
                sh("setfacl -d -m g::rwx /opt/softwareag2")
                withEnv(["wm_image=${params.wm_image}${params.wm_tag}",
                         "gateway_image=${params.gateway_image}${params.gateway_tag}",
                         "KEYSTORE_DIR=${keystore_dir}"]) {
                    sh("docker stack deploy -c rcip_devops/mp_stack/mp.yml mp")
                }
            }
        }
    }
}