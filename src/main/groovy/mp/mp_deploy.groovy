#!groovy
package mp

def env_params = []

node(params.node) {
    stage("Prepare parameters") {
        params.each() { k -> env_params.add(k.toString()) }
        echo("Got parameters from build set up: ${env_params}")
    }

    stage("Prepare artifacts") {
        build job: "mp_prepare_artifacts", parameters: [string(name: 'node', value: params.node),
                                                        string(name: 'branch_mp', value: params.branch_mp),
                                                        string(name: 'release_package', value: params.release_package)
        ]
    }
    stage("Build dockers") {
        build job: "mp_build_dockers", parameters: [string(name: 'HOST_IP', value: params.HOST_IP),
                                                    string(name: 'node', value: params.node),
                                                    string(name: 'branch_rcip', value: params.branch_rcip),
                                                    string(name: 'keystore_file_id', value: params.keystore_sts_file_id),
                                                    string(name: 'branch_mp', value: params.branch_mp)]
    }

    stage("Deploy stack") {
        build job: "mp_deploy_stack", parameters: [string(name: 'HOST_IP', value: params.HOST_IP),
                                                   string(name: 'node', value: params.node),
                                                   string(name: 'release_package', value: params.release_package),
                                                   string(name: 'wm_image', value: params.wm_image),
                                                   string(name: 'wm_tag', value: params.wm_tag),
                                                   string(name: 'gateway_image', value: params.gateway_image),
                                                   string(name: 'gateway_tag', value: params.gateway_tag),
                                                   string(name: 'oracle_image', value: params.oracle_image),
                                                   string(name: 'ldap_image', value: params.ldap_image),
                                                   string(name: 'memcached_image', value: params.memcached_image),
                                                   string(name: 'process_image', value: params.process_image),
                                                   string(name: 'flow_image', value: params.flow_image),
                                                   string(name: 'rest_image', value: params.rest_image),
                                                   string(name: 'presentation_image', value: params.presentation_image),
                                                   string(name: 'mock_image', value: params.mock_image),
                                                   string(name: 'mp_abe_output_dir', value: params.mp_abe_output_dir)
        ]
    }
    stage("Update database") {
        build job: "mp_update_database", parameters: [string(name: 'HOST_IP', value: params.HOST_IP),
                                                      string(name: 'node', value: params.node),
                                                      string(name: 'branch_rcip', value: params.branch_rcip)]
    }

    stage("Update wm") {
        build job: "mp_update_webmethods", parameters: [string(name: 'HOST_IP', value: params.HOST_IP),
                                                        string(name: 'branch_mp', value: params.branch_mp),
                                                        string(name: 'branch_rcip', value: params.branch_rcip),
                                                         string(name: 'node', value: params.node),
                                                         string(name: 'wm_image', value: params.wm_image),
                                                         string(name: 'wm_tag', value: params.wm_tag),
                                                         string(name: 'mp_abe_output_dir', value: params.mp_abe_output_dir)]
    }
    stage("Update apigateway"){
        build job: "mp_update_apigateway", parameters: [string(name: 'HOST_IP', value: params.HOST_IP),
                                                        string(name: 'node', value: params.node),
                                                        string(name: 'branch_mp', value: params.branch_mp),
                                                        string(name: 'gateway_image', value: params.gateway_image),
                                                        string(name: 'gateway_tag', value: params.gateway_tag),
                                                        string(name: 'mp_gateway_abe_output_dir', value: params.mp_gateway_abe_output_dir)]
    }
    stage("Update services"){
        sh("docker service update mp_rest --force")
        sh("docker service update mp_process-services --force")
    }
}