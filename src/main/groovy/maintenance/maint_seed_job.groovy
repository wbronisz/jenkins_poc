package maintenance

def buildsToKeep = 30
def config_file_id = "3d9392d6-fb63-4d55-baeb-0e41086ea2d7"
//def env_file_id = "1538e4a9-e419-4c7a-b645-8654e421306d"
def env_file_id = "1538e4a9-e419-4c7a-b645-8654e421306d"
def keystore_file_id = "c3fdef88-db79-4290-a089-167165457826"
def keystore_sts_file_id = "14c95148-b84e-4b41-8ac6-506f03770aba"
def hosts = "rcip.dev - 138.201.174.153\nrcip.qa - 138.201.174.152\nrcip.tests - 94.130.26.227\nAWS_RC - 172.31.13.199"
def mp_hosts = "172.31.14.241 - MP_dev\n172.31.44.79 - MP_test\n172.31.26.164 - MP_EMS\n172.31.11.124 - MP_Concapps"
def mp_images = ["oracle":"138.201.174.153:15000/mp/oracle_db:initialized",
    "wm":"138.201.174.153:15000/rcip/wm",
    "ldap":"138.201.174.153:15000/rcip/open-ldap",
    "memcached":"memcached",
    "mock":"138.201.174.153:15000/mp/mp-level7-mocks",
    "gateway":"138.201.174.153:15000/rcip/api-gateway"]

pipelineJob('seed_build_dockers') {
    logRotator{
        numToKeep(buildsToKeep)
    }
    definition {
        cps {
            sandbox()
            script(readFileFromWorkspace("src/main/groovy/build/build_dockers.groovy"))
        }
    }
    parameters {
        stringParam("HOST_IP", "138.201.174.152")
        stringParam("path_to_properties", "../build_parameters.parameters")
        stringParam("path_to_source", "../rcip_code")
        stringParam("node_name","master", "rcip.dev\nrcip.qa\nrcip.tests\nAWS_RC")
        stringParam("config_file_id", "${config_file_id}", "Config file for rcip docker images")

    }
}

pipelineJob('seed_deploy_update_database') {
    description("Documentation: TODO")
    logRotator{
        numToKeep(buildsToKeep)
    }

    parameters {
        stringParam("HOST_IP", "138.201.174.152")
        stringParam("path_to_properties", "../build_parameters.parameters")
        stringParam("path_to_source", "../rcip_code")
        stringParam("node_name","master", "rcip.dev\nrcip.qa\nrcip.tests\nAWS_RC")
        stringParam("config_file_id", "${config_file_id}", "Config file for rcip docker images")
    }

    definition {
        cps {
            sandbox()
            script(readFileFromWorkspace("src/main/groovy/deploy/deploy_update_database.groovy"))
        }
    }
}

pipelineJob('seed_deploy_update_web_methods_service') {
    description("Documentation: TODO")
    logRotator{
        numToKeep(buildsToKeep)
    }
    parameters {
        stringParam("HOST_IP", "138.201.174.152")
        stringParam("is_login", "Administrator")
        stringParam("is_password", "manage")
        stringParam("is_timeout", "600", "time for waiting for IS to start (in seconds)")
        stringParam("is_hostname", "172.31.13.199", hosts)
        stringParam("dest.env.ip", "172.31.13.199", hosts)
        stringParam("is_port", "5555")
        stringParam("wm_install_dir", "/opt/softwareag")
//        stringParam("rcip_abe_output_dir", "/var/lib/docker/volumes/rcip_RCIP_ABE_Output/_data/")

        stringParam("rcip_abe_output_dir", "/tmp/rcip_abe_output")
        stringParam("um_url", "rcip-wm", "URL to Universalmessaging")
        stringParam("um_port", "9000", "Port of UniversalMessaging")
        stringParam("path_to_source", "../rcip_code")
        stringParam("path_to_properties", "../build_parameters.parameters")
        stringParam("node_name","master", "rcip.dev\nrcip.qa\nrcip.tests\nAWS_RC")
        stringParam("env_file_id", "${env_file_id}", "Config file for webmethods docker image")
        stringParam("DOCKER_CLIENT_TIMEOUT","240")
        stringParam("COMPOSE_HTTP_TIMEOUT","240")
        stringParam('release_branch', 'develop', 'e.g. release/1.1.0.0-X')

    }
    definition {
        cps {
            sandbox()
            script(readFileFromWorkspace("src/main/groovy/deploy/deploy_update_web_methods_service.groovy"))
        }
    }
}

pipelineJob('seed_build_process_service') {
    description("Documentation: TODO")
    logRotator{
        numToKeep(buildsToKeep)
    }
    parameters {
        stringParam("project_name", "rcip-process-services")
        stringParam("timeout", "600")
        stringParam("path_to_properties", "../build_parameters.parameters")
        stringParam("path_to_source", "../rcip_code")
        stringParam("node_name","master", hosts)
        stringParam("config_file_id", "${config_file_id}", "Config file for rcip docker images")


    }
    definition {
        cps {
            sandbox()
            script(readFileFromWorkspace("src/main/groovy/build/build_process_service.groovy"))
        }
    }
}

pipelineJob('seed_build_security_token_service') {
    parameters {
        stringParam("HOST_IP", "138.201.174.152")
        stringParam("path_to_properties", "../build_parameters.parameters")
        stringParam("path_to_source", "../rcip_code")
        stringParam("node_name","master", hosts)
        stringParam("config_file_id", "${config_file_id}", "Config file for rcip docker images")
        stringParam("keystore_file_id", "${keystore_file_id}")
        stringParam("project_name", "rcip-sts")
    }
    definition {
        cps {
            sandbox()
            script(readFileFromWorkspace("src/main/groovy/build/build_security_token_service.groovy"))
        }
    }
}

pipelineJob('seed_build_config_ui_service') {
    parameters {
        stringParam("path_to_properties", "../build_parameters.parameters")
        stringParam("path_to_source", "../rcip_code")
        stringParam("node_name","master", hosts)
        stringParam("config_file_id", "${config_file_id}", "Config file for rcip docker images")
        stringParam("keystore_file_id", "${keystore_file_id}")
        stringParam("project_name", "rcip-ui")
        stringParam("port", "9093")
    }
    definition {
        cps {
            sandbox()
            script(readFileFromWorkspace("src/main/groovy/build/build_config_ui.groovy"))
        }
    }
}

pipelineJob('seed_build_presentation_service') {
    parameters {
        stringParam("path_to_properties", "../build_parameters.parameters")
        stringParam("path_to_source", "../rcip_code")
        stringParam("node_name","master", hosts)
        stringParam("config_file_id", "${config_file_id}", "Config file for rcip docker images")
        stringParam("project_name", "rcip-presentation-services")
    }
    definition {
        cps {
            sandbox()
            script(readFileFromWorkspace("src/main/groovy/build/build_presentation_service.groovy"))
        }
    }
}
pipelineJob('seed_build_rest_service') {
    parameters {
        stringParam("path_to_properties", "../build_parameters.parameters")
        stringParam("path_to_source", "../rcip_code")
        stringParam("node_name","master", hosts)
        stringParam("config_file_id", "${config_file_id}", "Config file for rcip docker images")
        stringParam("project_name", "rcip-rest")
        stringParam("keystore_file_id", "${keystore_file_id}")
    }
    definition {
        cps {
            sandbox()
            script(readFileFromWorkspace("src/main/groovy/build/build_rest_service.groovy"))
        }
    }
}
pipelineJob('seed_build_event_service') {
    parameters {
        stringParam("path_to_properties", "../build_parameters.parameters")
        stringParam("path_to_source", "../rcip_code")
        stringParam("node_name","master", hosts)
        stringParam("config_file_id", "${config_file_id}", "Config file for rcip docker images")
    }
    definition {
        cps {
            sandbox()
            script(readFileFromWorkspace("src/main/groovy/build/build_event_service.groovy"))
        }
    }
}
pipelineJob('seed_build_flow_initiation_service') {
    description("Documentation: TODO")
    logRotator{
        numToKeep(buildsToKeep)
    }
    parameters {
        stringParam("project_name", "rcip-flow-initiation-services")
        stringParam("flowInitServicePort", "9091")
        stringParam("path_to_properties", "../build_parameters.parameters")
        stringParam("path_to_source", "../rcip_code")
        stringParam("node_name","master", hosts)
        stringParam("config_file_id", "${config_file_id}", "Config file for rcip docker images")
    }
    definition {
        cps {
            sandbox()
            script(readFileFromWorkspace("src/main/groovy/build/build_flow_initiation_service.groovy"))
        }
    }
}

pipelineJob('seed_build_prepare_artifacts') {
    parameters {
        stringParam('release_branch', 'develop', 'e.g. release/1.1.0.0-X')
        stringParam("path_to_properties", "../build_parameters.parameters")
        stringParam("path_to_source", "../rcip_code")
        stringParam("node_name","master", hosts)
        stringParam("release_package","false", "false - skip package deployment\ntrue - deploy packages")
        stringParam("env_file_id", "${env_file_id}", "Config file for webmethods docker image")
    }
    definition {
        cps {
            sandbox()
            script(readFileFromWorkspace("src/main/groovy/build/build_prepare_artifacts.groovy"))
        }
    }
}

pipelineJob('seed_deploy_prepare_release') {
    description("Injects image names to docker stack file. Docker stack file is located in rcip_devops project at rcip_devops/rcip_stack/rcip.yml\n" +
            "To manualy deploy the stack, set local environment variables in file 'rcip_stack_envs', export them and run 'docker stack deploy -c <stack_file> <stack_name>")
    logRotator{
        numToKeep(buildsToKeep)
    }
    parameters {
        stringParam('release_branch', 'develop', 'e.g. release/1.1.0.0-X')
        stringParam("node_name","rcip.qa", hosts)
        stringParam("release_package","false", "false - skip package deployment\ntrue - deploy packages")
        stringParam("rcip_abe_output_dir", "/home/rcip/jenkins/workspace/rcip_abe_output")
        stringParam("rcip_gateway_abe_output_dir","/home/rcip/jenkins/workspace/rcip_gateway_abe_output_dir")
        stringParam("wm_image", "138.201.174.153:15000/rcip/wm")
        stringParam("oracle_image", "rcip/oracle_image:initialized", "rcip/oracle_image:initialized\nrcip/oracle-db")
        stringParam("ldap_image", "138.201.174.153:15000/rcip/open-ldap:latest")
        stringParam("memcached_image", "memcached",
                "memcached\n" +
                        "059031578858.dkr.ecr.us-west-2.amazonaws.com/rcip/memcached:latest")
        stringParam("wso2_server_image",
                "138.201.174.153:15000/rcip/wso2-server:release",
                "059031578858.dkr.ecr.us-west-2.amazonaws.com/rcip/wso2-server:demo\n" +
                        "rcip/brs-service-qa:1.0.0" +
                        "138.201.174.153:15000/rcip/wso2-server:release")
        stringParam("flow_image", "rcip/flow-initiation-services")
        stringParam("presentation_image", "rcip/presentation-services")
        stringParam("rest_image", "rcip/rest")
        stringParam("process_image", "rcip/process-services")
        stringParam("sts_image", "rcip/sts")
        stringParam("gateway_image","138.201.174.153:15000/rcip/api-gateway")
        stringParam("mock_image", "138.201.174.153:15000/rcip-ext-systems-mocks/mock-services:latest")
        stringParam("ui_demo_image", "138.201.174.153:15000/rcip/demo:1.1.1.0",
                "138.201.174.153:15000/rcip/demo:1.1.1.0\n" +
                        "059031578858.dkr.ecr.us-west-2.amazonaws.com/rcip/demo:1.1.1.0_new")
        stringParam("ui_config_image", "rcip/config-ui")
        stringParam("jackrabbit_image", "138.201.174.153:15000/rcip/jackrabbit:release",
                "059031578858.dkr.ecr.us-west-2.amazonaws.com/rcip/jackrabbit:1.1\n" +
                        "138.201.174.153:15000/rcip/jackrabbit" +
                        "138.201.174.153:15000/rcip/jackrabbit:release")
        stringParam("mashzone_image","138.201.174.153:15000/rcip/mashzone:latest")

        stringParam('stsPort', '9094')
        stringParam('presentationServicePort', '9090')
        stringParam('flowInitServicePort', '9091')
        stringParam('eventServicesPort', '9096')
        stringParam('processServicePort', '9092')
        stringParam('restApiPort', '9093')
        stringParam('IS_Port', '5555')
        stringParam("path_to_properties", "../build_parameters.parameters")
        stringParam("path_to_source", "../rcip_code")
        stringParam("config_file_id", "${config_file_id}", "Config file for rcip docker images")
        stringParam("env_file_id", "${env_file_id}", "Config file for rcip docker images")
        stringParam("keystore_file_id", "${keystore_file_id}")
        stringParam('release_version', '', 'f.e. 1.1.0.0-8 - demanded for Jira update.' +
                '\n Leave blank, if do not want to update the Jira')
        stringParam('test_project_name', 'RCIP-smoke-test-project.xml', 'Specify project to run.' +
                '\nRCIP-smoke-test-project.xml' +
                '\nRCIP-system-test-project.xml')
        stringParam("test_release_branch", 'release/1.3.0.0', 'Branch to run tests from, e.g. release/1.1.0.0-8')
    }
    definition {
        cps {
            sandbox()
            script(readFileFromWorkspace("src/main/groovy/deploy/prepare_release.groovy"
            ))
        }
    }
}

pipelineJob("seed_deploy_run_services") {
    description("Performs stack deploy")
    parameters{
        stringParam("path_to_properties", "../build_parameters.parameters")
        stringParam("HOST_IP","138.201.174.152", hosts)
        stringParam("dest.env.ip", "138.201.174.152", hosts)
        stringParam("path_to_source", "../rcip_code")
        stringParam("node_name", "AWS_RC", hosts)
        stringParam("wm_image", "138.201.174.153:15000/rcip/wm")
        stringParam("gateway_image","138.201.174.153:15000/rcip/api-gateway")
        stringParam("oracle_image", "rcip/oracle_image:initialized", "rcip/oracle_image:initialized\nrcip/oracle-db")
        stringParam("ldap_image", "138.201.174.153:15000/rcip/open-ldap:latest")
        stringParam("memcached_image", "memcached",
                "memcached\n" +
                    "059031578858.dkr.ecr.us-west-2.amazonaws.com/rcip/memcached:latest")
        stringParam("wso2_server_image",
                "138.201.174.153:15000/rcip/wso2-server:release",
                "059031578858.dkr.ecr.us-west-2.amazonaws.com/rcip/wso2-server:demo\n" +
                    "rcip/brs-service-qa:1.0.0" +
                        "138.201.174.153:15000/rcip/wso2-server:release")
        stringParam("flow_image", "rcip/flow-initiation-services")
        stringParam("presentation_image", "rcip/presentation-services")
        stringParam("rest_image", "rcip/rest")
        stringParam("process_image", "rcip/process-services")
        stringParam("sts_image", "rcip/sts")
        stringParam("mock_image", "138.201.174.153:15000/rcip-ext-systems-mocks/mock-services:latest")
        stringParam("ui_demo_image", "138.201.174.153:15000/rcip/demo:1.1.1.0",
                "138.201.174.153:15000/rcip/demo:1.1.1.0\n" +
                    "059031578858.dkr.ecr.us-west-2.amazonaws.com/rcip/demo:1.1.1.0_new")
        stringParam("ui_config_image", "rcip/config-ui")
        stringParam("jackrabbit_image", "138.201.174.153:15000/rcip/jackrabbit:release",
                "059031578858.dkr.ecr.us-west-2.amazonaws.com/rcip/jackrabbit:1.1\n" +
                        "138.201.174.153:15000/rcip/jackrabbit" +
                        "138.201.174.153:15000/rcip/jackrabbit:release")
        stringParam("mashzone_image","138.201.174.153:15000/rcip/mashzone:latest")
    }
    logRotator{
        numToKeep(buildsToKeep)
    }
    definition {
        cps {
            sandbox()
            script(readFileFromWorkspace("src/main/groovy/deploy/deploy_run_services.groovy"))
        }
    }
}

pipelineJob('seed_deploy_update_apigateway') {
    logRotator {
        numToKeep(buildsToKeep)
    }
    definition {
        cps {
            sandbox()
            script(readFileFromWorkspace('src/main/groovy/deploy/deploy_update_apigateway.groovy'))
        }
    }
    parameters {
        stringParam("node_name","AWS_RC", hosts)
        stringParam("HOST_IP", "138.201.174.152")
        stringParam("path_to_source", "../rcip_code")
        stringParam("rcip_gateway_abe_output_dir","/home/ubuntu/workspace/rcip_gateway_abe_output_dir")

    }
}

pipelineJob("seed_deploy_update_services") {
    description("Performs docker service update")
    parameters{
        stringParam("HOST_IP", "138.201.174.152")
        stringParam("path_to_properties", "../build_parameters.parameters")
        stringParam("path_to_source", "../rcip_code")
        stringParam("node_name","AWS_RC", hosts)
    }
    logRotator{
        numToKeep(buildsToKeep)
    }
    definition {
        cps {
            sandbox()
            script(readFileFromWorkspace("src/main/groovy/deploy/deploy_update_services.groovy"))
        }
    }
}

pipelineJob("seed_deploy_wso_rules") {
    description("Runs script from source")
    parameters{
        stringParam("path_to_properties", "../build_parameters.parameters")
        stringParam("path_to_source", "../rcip_code")
        stringParam("node_name","AWS_RC", hosts)
    }
    logRotator{
        numToKeep(buildsToKeep)
    }
    definition {
        cps {
            sandbox()
            script(readFileFromWorkspace("src/main/groovy/deploy/deploy_wso_rules.groovy"))
        }
    }
}

pipelineJob('seed_qa_tests') {
    description('Documentation: https://roundcube.atlassian.net/wiki/x/G4DLAg')
    logRotator{
        numToKeep(buildsToKeep)
    }
    parameters {
        stringParam("environment","138.201.174.152", hosts)
        stringParam('project_name', 'RCIP-smoke-test-project.xml', 'Specify project to run.' +
                '\nRCIP-smoke-test-project.xml' +
                '\nRCIP-system-test-project.xml')
        stringParam('release_branch', '', 'Branch to run tests from, f.e. release/1.1.0.0-8')
        stringParam('release_version', '', 'f.e. 1.1.0.0-8 - demanded for Jira update.' +
                '\n Leave blank, if do not want to update the Jira')
        stringParam("project", "RCIP", "project name for version release in Jira update job")
        stringParam("project_test", "RCIP_TEST", "project name for version release o tests in Jira update job")
    }
    definition {
        cps {
            sandbox()
            script(readFileFromWorkspace("src/main/groovy/tests/qa_tests.groovy"))
        }
    }
}

pipelineJob('seed_qa_update_jira') {
    definition {
        cps {
            sandbox()
            script(readFileFromWorkspace("src/main/groovy/tests/qa_update_jira.groovy"))
        }
    }
    parameters {
        stringParam("release_version", "")
        stringParam("GIT_BRANCH", "")
        stringParam("report_path", "")
        stringParam("project", "RCIP")
        stringParam("project_test", "RCIP_TEST")
        stringParam("BUILD_NUMBER", "")
    }
}

pipelineJob('seed_integration_rest_service') {
    description("Documentation: TODO")
    logRotator {
        numToKeep(buildsToKeep)
    }
    definition {
        cps {
            sandbox()
            script(readFileFromWorkspace("src/main/groovy/tests/integration_rest_service.groovy"))
        }
    }
    parameters {
        stringParam("path_to_properties", "../build_parameters.parameters")
        stringParam("path_to_source", "../rcip_code")
        stringParam("node_name","master", "rcip.dev\nrcip.qa\nrcip.tests\nAWS_RC")
        stringParam("project_name","rcip-rest")
        stringParam("restApiPort","9093")
    }
}

pipelineJob('seed_integration_rest_service') {
    description("Documentation: TODO")
    logRotator {
        numToKeep(buildsToKeep)
    }
    definition {
        cps {
            sandbox()
            script(readFileFromWorkspace("src/main/groovy/tests/integration_process_service.groovy"))
        }
    }
    parameters {
        stringParam("path_to_properties", "../build_parameters.parameters")
        stringParam("path_to_source", "../rcip_code")
         stringParam("timeout", "40")
        stringParam("node_name","master", hosts)
    }
}

pipelineJob('seed_integration_flow_initiation_service') {
    description("Documentation: TODO")
    logRotator {
        numToKeep(buildsToKeep)
    }
    definition {
        cps {
            sandbox()
            script(readFileFromWorkspace("src/main/groovy/tests/integration_flow_initiation_service.groovy"))
        }
    }
    parameters {
        stringParam("path_to_properties", "../build_parameters.parameters")
        stringParam("path_to_source", "../rcip_code")
        stringParam("node_name","master", hosts)
    }
}

pipelineJob('seed_integration_web_methods_service') {
    description("Documentation: TODO")
    logRotator {
        numToKeep(buildsToKeep)
    }
    definition {
        cps {
            sandbox()
            script(readFileFromWorkspace("src/main/groovy/tests/integration_web_methods_service.groovy"))
        }
    }
    parameters {
        stringParam("path_to_properties", "../build_parameters.parameters")
        stringParam("path_to_source", "../rcip_code")
        stringParam("node_name","master", hosts)
    }
}

pipelineJob('seed_maint_license_report') {
    triggers {
//        cron('H 23 * * *')
    }
    definition {
        cps {
            sandbox()
            script(readFileFromWorkspace('src/main/groovy/maintenance/license_report.groovy'))
        }
    }
}

pipelineJob('aws_start_instance') {
    logRotator {
        numToKeep(buildsToKeep)
    }
    definition {
        cps {
            sandbox()
            script(readFileFromWorkspace('src/main/groovy/aws/aws_start_instance.groovy'))
        }
    }
    parameters {
        stringParam("instanceID", "i-06f2fc7179ea8bf6e", "i-06f2fc7179ea8bf6e - rcip-demo-test 52.37.51.156 \n" +
                "i-01fb030d8564b9604 - rcip-demo 52.33.184.160 \n" +
                "i-0ea59f9deb76dae28 - MP_dev 54.69.131.196\n" +
                "i-04e9a9db19be91948 - MP_test 34.212.100.132\n" +
                "i-092d5822cb973ce9d - rcip-rc 34.211.82.251")
    }
}

pipelineJob('aws_restart_service') {
    logRotator {
        numToKeep(buildsToKeep)
    }
    definition {
        cps {
            sandbox()
            script(readFileFromWorkspace('src/main/groovy/aws/aws_restart_service.groovy'))
        }
    }
    parameters {
        stringParam("DEMO_ENV", "demo-test", "demo-test - 52.37.51.156\n" +
                "demo - 52.33.184.160")
        stringParam("DOCKER_IMAGE", "138.201.174.153:15000/rcip/demo-tesla-portal")
        stringParam("DOCKER_TAG", "API_1.3.0.0")
        stringParam("SERVICE_NAME", "rcip_demo-tesla-portal-nginx")
    }
}

pipelineJob('aws_restart_stack') {
    logRotator {
        numToKeep(buildsToKeep)
    }
    definition {
        cps {
            sandbox()
            script(readFileFromWorkspace('src/main/groovy/aws/aws_restart_stack.groovy'))
        }
    }
    parameters {
        stringParam("DEMO_ENV", "demo-test", "demo-test - 52.37.51.156\n" +
                "demo - 52.33.184.160")
        stringParam("STACK_NAME", "rcip")
        stringParam("COMPOSE_PATH", "/root/rcip/rcip.yml")
    }
}

pipelineJob('mp_build_dockers') {
    logRotator {
        numToKeep(buildsToKeep)
    }
    definition {
        cps {
            sandbox()
            script(readFileFromWorkspace('src/main/groovy/mp/mp_build_dockers.groovy'))
        }
    }
    parameters {
        stringParam("node","MP_dev","MP_dev\nMP_test")
        stringParam("release_package","false", "false - skip package deployment\ntrue - deploy packages")
        stringParam("branch_mp","develop","branch to build marketplace")
        stringParam("branch_rcip","develop", "branch to build rcip")
        stringParam("keystore_sts_file_id","${keystore_sts_file_id}")
        stringParam("HOST_IP", "172.31.14.241", "${mp_hosts}")
        stringParam("config_file_id", "${config_file_id}", "Config file for docker images")
    }
}

pipelineJob('mp_DEPLOY') {
    logRotator {
        numToKeep(buildsToKeep)
    }
    definition {
        cps {
            sandbox()
            script(readFileFromWorkspace('src/main/groovy/mp/mp_deploy.groovy'))
        }
    }
    parameters {
        stringParam("node","MP_dev","MP_dev\nMP_test")
        stringParam("HOST_IP", "172.31.14.241", "${mp_hosts}")
        stringParam("branch_mp","develop","branch to build marketplace")
        stringParam("branch_rcip","develop", "branch to build")
        stringParam("release_package","false", "false - skip package deployment\ntrue - deploy packages")
        stringParam("mp_abe_output_dir","/home/ubuntu/workspace/mp_abe_output")
        stringParam("mp_gateway_abe_output_dir","/home/ubuntu/workspace/mp_gateway_abe_output_dir")
        stringParam("wm_image",mp_images.wm)
        stringParam("wm_tag",":updated")
        stringParam("oracle_image",mp_images.oracle)
        stringParam("ldap_image",mp_images.ldap)
        stringParam("memcached_image",mp_images.memcached)
        stringParam("process_image","mp/process-services")
        stringParam("flow_image","rcip/flow-initiation-services")
        stringParam("presentation_image","rcip/presentation-services")
        stringParam("rest_image","mp/rest")
        stringParam("rest_auth_image", "mp/rest-auth-server")
        stringParam("sts_image","rcip/sts")
        stringParam("mock_image",mp_images.mock)
        stringParam("gateway_image",mp_images.gateway)
        stringParam("gateway_tag",":updated")
        stringParam("config_file_id", "${config_file_id}", "Config file for docker images")
        stringParam("keystore_sts_file_id","${keystore_sts_file_id}")


    }
}

pipelineJob('mp_deploy_stack') {
    logRotator {
        numToKeep(buildsToKeep)
    }
    definition {
        cps {
            sandbox()
            script(readFileFromWorkspace('src/main/groovy/mp/mp_deploy_stack.groovy'))
        }
    }
    parameters {
        stringParam("node","MP_dev","MP_dev\nMP_test")
        stringParam("HOST_IP", "172.31.14.241", "${mp_hosts}")
        stringParam("wm_image",mp_images.wm)
        stringParam("wm_tag",":updated")
        stringParam("oracle_image",mp_images.oracle)
        stringParam("ldap_image",mp_images.ldap)
        stringParam("memcached_image",mp_images.memcached)
        stringParam("process_image","mp/process-services")
        stringParam("flow_image","rcip/flow-initiation-services")
        stringParam("presentation_image","rcip/presentation-services")
        stringParam("rest_image","mp/rest")
        stringParam("rest_auth_image", "mp/rest-auth-server")
        stringParam("sts_image","rcip/sts")
        stringParam("mock_image",mp_images.mock)
        stringParam("gateway_image",mp_images.gateway)
        stringParam("gateway_tag",":updated")
        stringParam("mp_abe_output_dir","/home/ubuntu/workspace/mp_abe_output")
    }
}

pipelineJob('mp_build_process_service') {
    logRotator {
        numToKeep(buildsToKeep)
    }
    definition {
        cps {
            sandbox()
            script(readFileFromWorkspace('src/main/groovy/mp/mp_process_service.groovy'))
        }
    }
    parameters {
        stringParam("project_name","mp-process-services")
        stringParam("node","MP_dev","MP_dev\nMP_test")
        stringParam("HOST_IP", "172.31.14.241", "${mp_hosts}")
        stringParam("config_file_id", "${config_file_id}", "Config file for docker images")
    }
}
pipelineJob('mp_build_rest_service') {
    logRotator {
        numToKeep(buildsToKeep)
    }
    definition {
        cps {
            sandbox()
            script(readFileFromWorkspace('src/main/groovy/mp/mp_rest_service.groovy'))
        }
    }
    parameters {
        stringParam("project_name","mp-rest-api")
        stringParam("node","MP_dev","MP_dev\nMP_test")
        stringParam("HOST_IP", "172.31.14.241", "${mp_hosts}")
        stringParam("config_file_id", "${config_file_id}", "Config file for docker images")
    }
}

pipelineJob('mp_build_rest_auth_server') {
    logRotator {
        numToKeep(buildsToKeep)
    }
    definition {
        cps {
            sandbox()
            script(readFileFromWorkspace('src/main/groovy/mp/mp_rest_auth_server.groovy'))
        }
    }
    parameters {
        stringParam("node","MP_dev","MP_dev\nMP_test")
        stringParam("config_file_id", "${config_file_id}", "Config file for docker images")
    }
}

pipelineJob('mp_prepare_artifacts') {
    logRotator {
        numToKeep(buildsToKeep)
    }
    definition {
        cps {
            sandbox()
            script(readFileFromWorkspace('src/main/groovy/mp/mp_prepare_artifacts.groovy'))
        }
    }
    parameters {
        stringParam("node","MP_dev","MP_dev\nMP_test")
        stringParam("branch_mp", "develop", "branch to build from")
        stringParam("release_package", "false", "Config file for docker images")
    }
}

pipelineJob('mp_build_presentation_services') {
    logRotator {
        numToKeep(buildsToKeep)
    }
    definition {
        cps {
            sandbox()
            script(readFileFromWorkspace('src/main/groovy/mp/mp_presentation_services.groovy'))
        }
    }
    parameters {
        stringParam("project_name","mp-presentation-services")
        stringParam("node","MP_dev","MP_dev\nMP_test")
        stringParam("HOST_IP", "172.31.14.241", "${mp_hosts}")
        stringParam("config_file_id", "${config_file_id}", "Config file for docker images")
    }
}

pipelineJob('mp_build_flow_initiation') {
    logRotator {
        numToKeep(buildsToKeep)
    }
    definition {
        cps {
            sandbox()
            script(readFileFromWorkspace('src/main/groovy/mp/mp_flow_initiation.groovy'))
        }
    }
    parameters {
        stringParam("project_name","mp-flow-initiation-services")
        stringParam("node","MP_dev","MP_dev\nMP_test")
        stringParam("branch_rcip","develop","branch to build rcip")
        stringParam("HOST_IP", "172.31.14.241", "${mp_hosts}")
        stringParam("config_file_id", "${config_file_id}", "Config file for docker images")
    }
}

pipelineJob('mp_update_database') {
    logRotator {
        numToKeep(buildsToKeep)
    }
    definition {
        cps {
            sandbox()
            script(readFileFromWorkspace('src/main/groovy/mp/mp_update_database.groovy'))
        }
    }
    parameters {
        stringParam("node","MP_dev","MP_dev\nMP_test")
        stringParam("branch_rcip","develop", "branch to run RCIP liquibase")
        stringParam("HOST_IP", "172.31.14.241", "${mp_hosts}")
    }
}

pipelineJob('mp_update_webmethods') {
    logRotator {
        numToKeep(buildsToKeep)
    }
    definition {
        cps {
            sandbox()
            script(readFileFromWorkspace('src/main/groovy/mp/mp_update_webmethods.groovy'))
        }
    }
    parameters {
        stringParam("branch_mp","develop","branch to build marketplace")
        stringParam("branch_rcip","develop","branch to build rcip")
        stringParam("node","MP_dev","MP_dev\nMP_test")
        stringParam("HOST_IP", "172.31.14.241", "${mp_hosts}")
        stringParam("wm_image",mp_images.wm)
        stringParam("wm_tag",":updated")
        stringParam("mp_abe_output_dir","/home/ubuntu/workspace/mp_abe_output")

    }
}

pipelineJob('mp_update_apigateway') {
    logRotator {
        numToKeep(buildsToKeep)
    }
    definition {
        cps {
            sandbox()
            script(readFileFromWorkspace('src/main/groovy/mp/mp_update_apigateway.groovy'))
        }
    }
    parameters {
        stringParam("node","MP_dev","MP_dev\nMP_test")
        stringParam("HOST_IP", "172.31.14.241", "${mp_hosts}")
        stringParam("gateway_image",mp_images.gateway)
        stringParam("gateway_tag",":updated")
        stringParam("mp_gateway_abe_output_dir","/home/ubuntu/workspace/mp_gateway_abe_output_dir")
        stringParam("branch_mp","develop","branch to build marketplace")
    }
}

categorizedJobsView('deploy') {
    jobs {
        regex(/seed_deploy_.*/)
    }
    columns {
        buildButton()
        status()
        weather()
        name()
        lastSuccess()
        lastFailure()
        lastBuildConsole()
        lastDuration()
        cronTrigger()
        configureProject()

    }
}

categorizedJobsView('builds') {
    jobs {
        regex(/seed_build_.*/)
    }
    columns {
        buildButton()
        status()
        weather()
        name()
        lastSuccess()
        lastFailure()
        lastBuildConsole()
        lastDuration()
        cronTrigger()
        configureProject()

    }
}

categorizedJobsView('tests') {
    jobs {
        regex("seed_integration_.*|seed_qa_.*")
    }
    columns {
        buildButton()
        status()
        weather()
        name()
        lastSuccess()
        lastFailure()
        lastBuildConsole()
        lastDuration()
        cronTrigger()
        configureProject()
    }
}

categorizedJobsView('maintenance') {
    jobs {
        regex(/seed_maint_.*/)
    }
    columns {
        buildButton()
        status()
        weather()
        name()
        lastSuccess()
        lastFailure()
        lastBuildConsole()
        lastDuration()
        cronTrigger()
        configureProject()
    }

}

categorizedJobsView('aws environments') {
    jobs {
        regex(/aws_.*/)
    }
    columns {
        buildButton()
        status()
        weather()
        name()
        lastSuccess()
        lastFailure()
        lastBuildConsole()
        lastDuration()
        cronTrigger()
        configureProject()
    }

}

categorizedJobsView('marketplace') {
    jobs {
        regex(/mp_.*/)
    }
    columns {
        buildButton()
        status()
        weather()
        name()
        lastSuccess()
        lastFailure()
        lastBuildConsole()
        lastDuration()
        cronTrigger()
        configureProject()
    }

}

