package maintenance


/*
node ('rcip.qa') {
    stage ('Checkout') {
        git changelog: false, credentialsId: 'bitbucket', poll: false, url: 'https://rcip-jenkins@bitbucket.org/RCIP/rcip_dev.git', branch: 'develop'
    }
    stage ('Gather licenses') {
        dir('source') {
            sh './show_deps.sh'
        }
    }
    stage ('Prepare and upload license file') {
        dir('source'){
            sh 'python deps_parse.py /tmp/rcip.deps.txt'
        }
    }
}*/

def source_path
def params_list = []
node ('rcip.qa') {
    stage ('Checkout') {
        checkout changelog: false, poll: false, scm:
                [$class: 'GitSCM',
                 branches: [[name: "develop"]],
                 doGenerateSubmoduleConfigurations: false,
                 extensions: [[$class: 'RelativeTargetDirectory',
                               relativeTargetDir: '../rcip_code']],
                 submoduleCfg: [],
                 userRemoteConfigs: [[credentialsId: "bitbucket", url: "https://rcip-jenkins@bitbucket.org/RCIP/rcip_dev.git"]]
                ]
        checkout changelog: false, poll: false, scm:
                [$class: 'GitSCM',
                 branches: [[name: "master"]],
                 doGenerateSubmoduleConfigurations: false,
                 extensions: [[$class: 'RelativeTargetDirectory',
                               relativeTargetDir: '../rcip_devops']],
                 submoduleCfg: [],
                 userRemoteConfigs: [[credentialsId: "bitbucket", url: "https://rcip-jenkins@bitbucket.org/RCIP/rcip_devops.git"]]
                ]
    }
    stage ('Gather licenses') {
        dir('../rcip_code') {
            source_path = pwd()
            params_list.add("RCIP_SOURCE_PATH=${source_path}/source")
            print params_list
        }
        withEnv(params_list){
            dir('../rcip_devops/license_scripts') {
                sh("./show_deps.sh")
            }
        }
    }
    stage ('Prepare and upload license file') {
        dir('../rcip_devops/license_scripts'){
            sh 'python get_licenses.py'
        }
    }
    stage ('Notify if new license is present') {
        dir('../rcip_devops/license_scripts'){
            if (fileExists('diff.txt')) {
                difference = readFile file: 'diff.txt'
                print difference
                mail to: 'wbronisz@7bulls.com', subject: 'New license has been added', body: "New license has been found in the project: \n ${difference}.\nPlease check the policy at https://roundcube.atlassian.net/wiki/spaces/OSLO/overview"
                sh "rm diff.txt"
            }
        }
    }
}